import React from 'react';
import Button from './components/Button/Button.jsx';
import Modal from './components/Modal/Modal.jsx';
import './App.css';

export default class App extends React.Component {
  state = {
    isFirstModalOpen: false,
    isSecondModalOpen: false,
  };

  openFirstModal = () => {
    this.setState({ isFirstModalOpen: true });
  };

  openSecondModal = () => {
    this.setState({ isSecondModalOpen: true });
  };

  closeModal = () => {
    this.setState({ 
      isFirstModalOpen: false,
      isSecondModalOpen: false  
    });
  };

  handleOutsideClick = (event) => {
    // Перевіряємо, чи клік був здійснений за межами модального вікна
    if (event.currentTarget === event.target) {
      //Якщо так, то додаємо код для закриття модального вікна
      this.setState({ 
        isFirstModalOpen: false,
        isSecondModalOpen: false  
      });
    }
  };

  render() {
    return (
      <div className="btn-wrapper" >
        <Button
          optionalСlassName="btn-primary"
          backgroundColor="yellow"
          text="Open first modal"
          onClick={this.openFirstModal}
        />
        <Button
          optionalСlassName="btn-secondary"
          backgroundColor="blue"
          text="Open second modal"
          onClick={this.openSecondModal}
        />

        {this.state.isFirstModalOpen && (
          <Modal
            header="Do you want to delete this file?"
            closeButton={true}
            closeModal={this.closeModal}
            handleOutsideClick={this.handleOutsideClick}
            text="Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?"
            actions={
              <div className="button-container">
                  <Button 
                  className="btn" 
                  onClick={this.closeModal} 
                  text="Ok" 
                  />
                  <Button 
                  className="btn" 
                  onClick={this.closeModal}
                  text="Cancel"
                  />
              </div>
            }
          />
        )}

        {this.state.isSecondModalOpen && (
          <Modal
            header="Home Work 1 - MODAL-WINDOW"
            closeButton={true}
            closeModal={this.closeModal}
            handleOutsideClick={this.handleOutsideClick}
            text="How would you rate my work?"
            actions={
              <div className="button-container">
                <Button 
                  optionalСlassName="btn-primary" 
                  backgroundColor="yellow"
                  onClick={this.closeModal}
                  text="GREAT!"
                />
                <Button
                  optionalСlassName="btn-secondary" 
                  backgroundColor="blue" 
                  onClick={this.closeModal}
                  text="Fine"
                />
                <Button 
                  onClick={this.closeModal}
                  text="so-so"
                />
              </div>
            }
          />
        )}
      </div>
    );
  }
}

